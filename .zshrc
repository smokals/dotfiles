export ZSH="/home/lucas/.oh-my-zsh"

ZSH_THEME="fishy"

alias tmux="TERM=screen-256color-bce tmux"
alias vf="vim \$(fzf -i --preview='head -100 {}')"
alias vimrc="vim ~/.vimrc"
alias zshrc="vim ~/.zshrc"
alias ff="firefox"
alias nightly="firefox-nightly"
alias sl="ls"
alias cl="clear"
alias t="todo.sh"
alias config="/usr/bin/git --git-dir=/home/lucas/.cfg/ --work-tree=/home/lucas"
alias yt-dl="youtube-dl -x --audio-format flac --audio-quality 0"

# make _ and - interchangeable in completion
HYPHEN_INSENSITIVE="true"

# ENABLE_CORRECTION="true"
COMPLETION_WAITING_DOTS="true"

HIST_STAMPS="yyyy-mm-dd"

plugins=(
  git
  tmux
)

source $ZSH/oh-my-zsh.sh

#export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='vim'
fi

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

