call plug#begin()
Plug 'vim-airline/vim-airline'
Plug 'scrooloose/syntastic'
Plug 'terryma/vim-multiple-cursors'
Plug 'junegunn/fzf', {'dir': '~/.fzf', 'do': './install --all'}
Plug 'junegunn/fzf.vim'
Plug 'scrooloose/nerdtree'
Plug 'jiangmiao/auto-pairs'
Plug 'scrooloose/nerdcommenter'
Plug 'ntpeters/vim-better-whitespace'
Plug 'lervag/vimtex'
Plug 'vimwiki/vimwiki'
Plug 'airblade/vim-gitgutter'

Plug 'arcticicestudio/nord-vim'
Plug 'dracula/vim', {'as': 'dracula'}
call plug#end()

let g:nord_bold_vertical_split_line=1
set background=dark
colorscheme dracula


set number
set relativenumber
set cursorline
set cursorcolumn
set colorcolumn=100
set autochdir
set autoindent
set noshowmode

syntax on

set tabstop=4
set shiftwidth=4
set expandtab

set backup
set backupdir=~/vimtmp/
set directory=~/vimtmp/

filetype on

" don't create .viminfo
set viminfo=

set encoding=utf-8

command LatexCompile execute "!pdflatex %"
command Mupdf execute "!mupdf %:r.pdf"

" C-h & C-l for tab switching
nnoremap <C-h> gT
nnoremap <C-l> gt

map ; :Files<cr>
nnoremap <f5> :NERDTreeToggle<cr>
nnoremap <f6> :!make<cr>
nnoremap <f9> :LatexCompile<cr>
nnoremap <f10> :Mupdf<cr>

" open splits below & to the right of the current window
set splitbelow splitright

" cursor shapes
let &t_SI="\<Esc>[6 q"
let &t_SR="\<Esc>[4 q"
let &t_EI="\<Esc>[2 q"

" plugin config
let NERDTreeMinimalUI=1
let NERDTreeDirArrors=1
let g:airline#extensions#tabline#enabled=1
let g:vimwiki_list=[{"path": "~/vimwiki/", "syntax": "markdown", "ext": ".md"}]
let g:vimtex_view_method="mupdf"


let g:airline_powerline_fonts = 1
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif
let g:airline_left_sep = ''
let g:airline_right_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''

let g:NERDSpaceDelims=1            " add a space after comment delimiters
let g:NERDCompactSexyComs=1        " use compact syntax
let g:NERDCommentEmptyLines=1      " allow commenting empty lines
let g:NERDTrimTrailingWhitespace=1 " remove trailing whitespace after uncommenting
let g:NERDToggleCheckAllLines=1    " check all selected lines for preexisting commenting

" syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

