set $mod Mod4
#set $mod Mod1

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:DejaVu Sans Mono 8

# Before i3 v4.8, we used to recommend this one as the default:
# font -misc-fixed-medium-r-normal--13-120-75-75-C-70-iso10646-1
# The font above is very space-efficient, that is, it looks good, sharp and
# clear in small sizes. However, its unicode glyph coverage is limited, the old
# X core fonts rendering does not support right-to-left and this being a bitmap
# font, it doesn’t scale on retina/hidpi displays.

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
bindsym $mod+Return exec urxvt

# kill focused window
bindsym $mod+Shift+q kill

# start dmenu (a program launcher)
# bindsym $mod+d exec dmenu_run -i
bindsym $mod+d exec rofi -show drun
# There also is the (new) i3-dmenu-desktop which only displays applications
# shipping a .desktop file. It is a wrapper around dmenu, so you need that
# installed.
# bindsym $mod+d exec --no-startup-id i3-dmenu-desktop

# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation (NOTE: $mod+g not default $mod+h)
bindsym $mod+g split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1: tmux"
set $ws2 "2: editor"
set $ws3 "3: file manager"
set $ws4 "4: task"
set $ws5 "5: SSH"
set $ws6 "6: misc"
set $ws7 "7: misc"
set $ws8 "8: chat"
set $ws9 "9: music"
set $ws10 "10: browser"

# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize"
{
	# These bindings trigger as soon as you enter the resize mode

	# Pressing left will shrink the window’s width.
	# Pressing right will grow the window’s width.
	# Pressing up will shrink the window’s height.
	# Pressing down will grow the window’s height.
	bindsym h resize shrink width 10 px or 10 ppt
	bindsym j resize grow height 10 px or 10 ppt
	bindsym k resize shrink height 10 px or 10 ppt
	bindsym l resize grow width 10 px or 10 ppt

	# same bindings, but for the arrow keys
	bindsym Left resize shrink width 10 px or 10 ppt
	bindsym Down resize grow height 10 px or 10 ppt
	bindsym Up resize shrink height 10 px or 10 ppt
	bindsym Right resize grow width 10 px or 10 ppt

	# back to normal: Enter or Escape or $mod+r
	bindsym Return mode "default"
	bindsym Escape mode "default"
	bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"

# colours
set $bg-color 	         #2f343f
set $inactive-bg-color   #2f343f
set $text-color          #f3f4f5
set $inactive-text-color #676E7D
set $urgent-bg-color     #E53935

# window colors
#                       border              background         text                 indicator
client.focused          $bg-color           $bg-color          $text-color          #800080
client.unfocused        $inactive-bg-color  $inactive-bg-color $inactive-text-color #800080
client.focused_inactive $inactive-bg-color  $inactive-bg-color $inactive-text-color #800080
client.urgent           $urgent-bg-color    $urgent-bg-color   $text-color          #800080

bar
{
	# status_command i3status
	status_command i3blocks -c ~/.config/i3/i3blocks.conf
	# tray_output none
	colors
	{
		background $bg-color
		separator #757575
		#                  border             background         text
		focused_workspace  $bg-color          $bg-color          $text-color
		inactive_workspace $inactive-bg-color $inactive-bg-color $inactive-text-color
		urgent_workspace   $urgent-bg-color   $urgent-bg-color   $text-color
	}
}

# bind mod+shift+x to lock, -c is the colour and -f to show tries
bindsym $mod+shift+x exec i3lock -c 1D262A -f

# laptop volume controls
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume 0 +5%
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume 0 -5%
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 0 toggle
# laptop brightness controls
bindsym XF86MonBrightnessUp exec xbacklight -inc 5
bindsym XF86MonBrightnessDown exec xbacklight -dec 5

# set wallpaper
# TODO: env variable to get username?
exec_always feh --bg-scale /home/lucas/pictures/backgrounds/bg.png

# load / refresh .Xresources file
exec_always --no-startup-id xrdb ~/.Xresources

exec --no-startup-id compton

# network manager applet
exec --no-startup-id nm-applet

# run programs on startup
# (none right now)

# screenshots
# $mod+Shift+Print for a region screenshot
# $mod+Print to screenshot whole screen
# bindsym $mod+shift+F1 exec escrotum "~/Pictures/screenshots/$(date)_screenshot.png"
bindsym $mod+shift+Print exec xfce4-screenshooter -r -o "xclip -selection clipboard -t image/png"
bindsym $mod+Print exec xfce4-screenshooter -f -o "xclip -selection clipboard -t image/png"

# bind programs to workspaces
assign [class="Hexchat"] $ws8
assign [class="discord"] $ws8
assign [class="telegram"] $ws8
assign [class="Firefox"] $ws10
assign [class="Firefox-esr"] $ws10
assign [class="Nightly"] $ws10

# when mod+shift+enter is pressed, open a floating terminal
bindsym $mod+shift+Return exec urxvt -name floating
for_window [class="URxvt" instance="floating"] floating enable

# thin borders
hide_edge_borders both
